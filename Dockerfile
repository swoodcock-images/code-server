# Collect binaries
ARG EXTERNAL_REG
ARG PYTHON_VERSION
ARG NODE_TAG
FROM ${EXTERNAL_REG}/node:${NODE_TAG}-bullseye-slim as node-img



FROM ${EXTERNAL_REG}/python:${PYTHON_VERSION}-slim-bookworm

ARG PYTHON_VERSION
ARG NODE_TAG
ARG GIT_COMMIT
ARG MAINTAINER
LABEL "APP_USER"="${APP_USER}" \
      "GIT_COMMIT"="${GIT_COMMIT}" \
      "CODE_SERVER_VERSION"="${TAG}" \
      "PYTHON_VERSION"="${PYTHON_VERSION}" \
      "NODE_VERSION"="${NODE_TAG}" \
      "MAINTAINER"="${MAINTAINER}"

SHELL ["/bin/bash", "-c"]

ARG http_proxy
ARG https_proxy

RUN set -ex \
    && RUN_DEPS=" \
    curl \
    dumb-init \
    fish \
    htop \
    man \
    nano \
    git \
    procps \
    openssh-client \
    sudo \
    lsb-release \
    apt-transport-https \
    gnupg-agent \
    gnupg2 \
    libpq-dev \
    build-essential \
    libbtrfs-dev \
    libgpgme-dev \
    libdevmapper-dev \
    pkg-config \
    " \
    && apt-get update \
    && apt-mark hold ca-certificates \
    && apt-get install -y --no-install-recommends $RUN_DEPS \
    && rm -rf /var/lib/apt/lists/*

# Change Shell to Fish
RUN chsh -s /usr/bin/fish
ENV SHELL /usr/bin/fish

# Create non-root user to run the app
ARG APP_USER=coder
ENV BUILD_USER=$APP_USER
RUN adduser --gecos '' --disabled-password ${APP_USER} && \
    echo "${APP_USER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/nopasswd

# Install code-server
ARG TAG
RUN curl -fsSL https://code-server.dev/install.sh | sh -s -- --version=${TAG}
# Set Node to use system CA Certs, otherwise downloads error as self-signed
ENV NODE_EXTRA_CA_CERTS=/etc/ssl/certs/ca-certificates.crt
# Add extensions (must use --extra-extensions-dir=/opt/code-server/extensions)
RUN code-server --install-extension ms-python.python && \
    code-server --install-extension ms-azuretools.vscode-docker && \
    code-server --install-extension ms-kubernetes-tools.vscode-kubernetes-tools && \   
    code-server --install-extension golang.go && \
    code-server --install-extension mikestead.dotenv && \
    code-server --install-extension redhat.vscode-yaml && \
    code-server --install-extension antfu.vite && \
    code-server --install-extension johnsoncodehk.volar && \
    code-server --install-extension dbaeumer.vscode-eslint && \
    code-server --install-extension antfu.iconify && \
    code-server --install-extension lokalise.i18n-ally && \
    code-server --install-extension vscode-icons-team.vscode-icons && \
    code-server --install-extension svelte.svelte-vscode && \
    curl -L \
      "https://marketplace.visualstudio.com/_apis/public/gallery/publishers/GitHub/vsextensions/copilot/latest/vspackage" \
      -o github.copilot.vsix.gz && \
    gunzip -v github.copilot.vsix.gz && \
    code-server --install-extension ./github.copilot.vsix && \
    rm -rf github.copilot.vsix && \
    # Make configs dir for run with no UID/GID
    mkdir -p /opt/code-server/configs && \
    mv /root/.local/share/code-server/extensions /opt/code-server/ && \
    chown -R coder:coder /opt/code-server/extensions && \
    rm -rf /root/.local

# # Install QGIS
# RUN curl -sL https://qgis.org/downloads/qgis-2021.gpg.key | gpg --import - && \
#     gpg --fingerprint 46B5721DBBD2996A && \
#     gpg --export --armor 46B5721DBBD2996A | apt-key add - && \
#     echo "deb https://qgis.org/debian-ltr $(lsb_release -cs) main" >> /etc/apt/sources.list && \
#     apt-get update && \
#     apt-get install -y --no-install-recommends \
#     qgis \
#     python3-qgis \
#     qgis-common \
#     qgis-providers \
#     && rm -rf /var/lib/apt/lists/*
# # Set dummy virtual display as USER (for PyQt5/qgis)
# # x11dummy volume mounted, set DISPLAY to match
# ENV DISPLAY=:1

# Add NodeJS + NPM
COPY --from=node-img \
    /usr/local/bin/node /usr/local/bin/node
COPY --from=node-img \
    /usr/local/lib/node_modules /usr/local/lib/node_modules
RUN ln -s /usr/local/bin/node /usr/local/bin/nodejs \
    && ln -s /usr/local/lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npm \
    && npm install --location=global pnpm @quasar/cli

# Install Golang (cannot add MUSL / Alpine compiled Go binary!)
ARG GOLANG_VERSION
RUN curl -fsSL "https://golang.org/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz" | tar xz -C /usr/local
ENV PATH "/usr/local/go/bin:$PATH"

# Port for code-server
EXPOSE 8888

# Break cache to force JupyterHub re-pull
ARG CACHEBREAK
# Add entrypoint script to root-owned dir
ADD docker-entrypoint.sh /docker-entrypoint.sh

# Essential Python dev deps
RUN pip install --user pdm pre-commit commitizen \
    && mkdir -p /opt/python \
    && mv /root/.local/bin /opt/python/ \
    && mv "/root/.local/lib/python$PYTHON_VERSION/site-packages" /opt/python/pkgs \
    && chown -R ${APP_USER} /opt \
    && chmod -R 777 /opt
ENV PATH="/opt/python/bin:$PATH" \
    PYTHONPATH="/opt/python/pkgs:/opt/python/pkgs/pdm/pep582"

# Switch to non-root user, add local .py files and run
USER ${APP_USER}
WORKDIR /home/${APP_USER}

# Proxy / SSL for Git, NPM
ARG LOCAL_GITLAB_URL
RUN set -euo pipefail && \
    git config --global credential.helper store && \
    git config --global pull.rebase false && \
    git config --global url."https://github.com/".insteadOf git@github.com: && \
    git config --global url."https://".insteadOf git:// && \
    if [[ -z "$http_proxy" ]] ; then \
        echo "No http_proxy set, skipping (Git, NPM, pnpm) config." ; \
    else \
        git config --global http.proxy "${http_proxy}" && \
        git config --global https.proxy "${https_proxy}" && \
        git config --global http.sslCAInfo /etc/ssl/certs/ca-certificates.crt && \
        if [[ -z "$LOCAL_GITLAB_URL" ]] ; then \
            echo "ARG LOCAL_GITLAB_URL not set, skipping Git no_proxy config." ; \
        else \
            git config --global http."${LOCAL_GITLAB_URL}".proxy "" && \
            git config --global https."${LOCAL_GITLAB_URL}".proxy "" ; \
        fi && \
        npm config set proxy "${http_proxy}" && \
        npm config set https-proxy "${https_proxy}" && \
        npm config set no-proxy "${no_proxy}" && \
        npm config set cafile /etc/ssl/certs/ca-certificates.crt && \
        pnpm config set proxy "${http_proxy}" && \
        pnpm config set https-proxy "${https_proxy}" && \
        pnpm config set no-proxy "${no_proxy}" && \
        pnpm config set cafile /etc/ssl/certs/ca-certificates.crt ; \
    fi && \
    pnpm config set auto-install-peers true

# Entrypoint script, passing commands to script as $@
ENTRYPOINT ["/docker-entrypoint.sh", "--bind-addr", "0.0.0.0:8888", \
            "--auth", "none", "--disable-telemetry", "--disable-update-check", "."]
