# code-server

### Build
- Modify .env
- Build with `bash build_script.sh`

### Deploy
- Modify docker-compose.yaml with desired users
- Deploy with `docker-compose up -d`

### Config
- To use the PyQGIS API:
  - Uncomment the section in the Dockerfile
  - Uncomment in docker-compose.yaml:
    - x11_link volume 
    - x11_dummy service
    - x11_link volume mapping is x-vscode-common
