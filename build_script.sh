#!/bin/bash
set -euo pipefail

export DOCKER_BUILDKIT=1

echo "Building VSCode"
GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
GIT_COMMIT=$(git rev-parse --short HEAD)
source .env

docker build --pull \
    --tag "${INTERNAL_REG}/vscode:${TAG}-${GIT_BRANCH}" . \
    --build-arg TAG=${TAG} \
    --build-arg http_proxy=$PROXY_URL \
    --build-arg https_proxy=$PROXY_URL \
    --build-arg GIT_COMMIT=${GIT_COMMIT} \
    --build-arg DOCKER_HOST=${DOCKER_HOST} \
    --build-arg DOCKER_TAG=${DOCKER_TAG} \
    --build-arg NODE_TAG=${NODE_TAG} \
    --build-arg DOCKER_COMPOSE_VERSION=${DOCKER_COMPOSE_VERSION} \
    --build-arg KUBECTL_TAG=${KUBECTL_TAG} \
    --build-arg GOLANG_VERSION=${GOLANG_VERSION} \
    --build-arg PYTHON_VERSION=${PYTHON_VERSION} \
    --build-arg LOCAL_GITLAB_URL=${LOCAL_GITLAB_URL} \
    --build-arg MAINTAINER=${MAINTAINER} \
    --build-arg INTERNAL_REG=${INTERNAL_REG} \
    --build-arg EXTERNAL_REG=${EXTERNAL_REG} \
    --build-arg CACHEBREAK="$(date +%s)"

if [[ "${PUSH_IMAGE}" = true ]] ; then
   echo "Pushing ${INTERNAL_REG}/vscode:${TAG}-${GIT_BRANCH}"
   docker push "${INTERNAL_REG}/vscode:${TAG}-${GIT_BRANCH}" ;
else
   echo "Skipped pushing ${INTERNAL_REG}/vscode:${TAG}-${GIT_BRANCH}" ;
fi

echo "Tagging image main branch image as :latest"
docker tag \
      "${INTERNAL_REG}/vscode:${TAG}-main" \
      "${INTERNAL_REG}/vscode:latest"
