#!/bin/bash

# Don't set -u, or unset vars cause exit (e.g. KUBE_DEPLOY)
set -eo pipefail



### FUNCTIONS START ###
handle_secrets () {
  # Kubeconfig secret
  if [[ -f "/run/secrets/kube_config" ]]; then
    if [[ -f "/home/${NB_USER}/.kube/config" ]]; then
      echo "Deleting existing /home/${NB_USER}/.kube/config"
      rm "/home/${NB_USER}/.kube/config"
    fi
    echo "Copying kube_config secret to ~/.kube/config"
    mkdir -p "/home/${NB_USER}/.kube"
    cat /run/secrets/kube_config > "/home/${NB_USER}/.kube/config"
    chown -R "${NB_UID}":"${NB_GID}" "/home/${NB_USER}/.kube"
    chmod 600 "/home/${NB_USER}/.kube/config"
  else
    echo "kube_config secret not present. Set the KUBE_CONFIG_FILE variable to use Kubernetes"
  fi
  # Remote Docker machine private key secret
  if [[ -f "/run/secrets/id_rsa" ]]; then
    if [[ -f "/home/${NB_USER}/.ssh/id_rsa" ]]; then
      echo "Deleting existing /home/${NB_USER}/.ssh/id_rsa"
      rm "/home/${NB_USER}/.ssh/id_rsa"
    fi
    echo "Copying /run/secrets/id_rsa to ~/.ssh/id_rsa"
    mkdir -p "/home/${NB_USER}/.ssh"
    cat /run/secrets/id_rsa > "/home/${NB_USER}/.ssh/id_rsa"
    chown -R "${NB_UID}":"${NB_GID}" "/home/${NB_USER}/.ssh"
    chmod 600 "/home/${NB_USER}/.ssh/id_rsa"
  else
    echo "id_rsa secret not present. Set it to use Docker"
  fi
  # Docker config.json secret
  if [[ -f "/run/secrets/config.json" ]]; then
    if [[ -f "/home/${NB_USER}/.docker/config.json" ]]; then
      echo "Deleting existing /home/${NB_USER}/.docker/config.json"
      rm "/home/${NB_USER}/.docker/config.json"
    fi
    echo "Copying /run/secrets/config.json to ~/.docker/config.json"
    mkdir -p "/home/${NB_USER}/.docker"
    cat /run/secrets/config.json > "/home/${NB_USER}/.docker/config.json"
    chown -R "${NB_UID}":"${NB_GID}" "/home/${NB_USER}/.docker"
    chmod 600 "/home/${NB_USER}/.docker/config.json"
  else
    echo "Docker config.json secret not present. Set it to use Harbor registry"
  fi
}

handle_kubernetes () {
  # Set user-data-dir based on Docker/Kubernetes deploy
  if [[ ! -z "${KUBE_DEPLOY}" ]]; then
    echo "Kubernetes deployment. Set user-data-dir to /home/${NB_USER}"
    export DATA_DIR="/home/${NB_USER}"
  else
    echo "Docker deployment. Set user-data-dir to /opt/code-server/configs/${NB_USER}"
    export DATA_DIR="/opt/code-server/configs/${NB_USER}"
    echo "Creating ${DATA_DIR}"
    mkdir -p "${DATA_DIR}"
    # Copy generic extensions to user dir
    if [[ ! -d "${DATA_DIR}/extensions" ]]; then
      echo "${DATA_DIR}/extensions does not exist. Populating with generic extensions..."
      mv "/opt/code-server/extensions" "${DATA_DIR}"
    fi
    # If UID:GID provided, set correct permissions on DATA_DIR
    if [[ ! -z "${NB_UID}" && ! -z "${NB_GID}" ]]; then
      echo "Changing permissions of ${DATA_DIR} to ${NB_UID}:${NB_GID}"
      chown -R "${NB_UID}":"${NB_GID}" "${DATA_DIR}"
    else
      echo "Changing permissions of ${DATA_DIR} to ${NB_USER}:root"
      chown -R "${NB_USER}" "${DATA_DIR}"
    fi
    echo "Setting permissions of docker.sock to 777 for non-root users."
    # Allow to fail if docker.sock isn't mounted
    chmod 777 /var/run/docker.sock || true
  fi
}

run_code_server () {
  exec sudo -E -H -u "$1" \
      PATH="${PATH}" \
      XDG_CACHE_HOME="/home/$1/.cache" \
      PYTHONPATH="${PYTHONPATH:-}" \
    dumb-init /usr/bin/code-server \
      --user-data-dir "$2" "${@:3}"
}
### FUNCTIONS END #



if [ "$(id -u)" == 0 ] ; then

    if [[ ! -z "${NB_USER}" ]]; then

        if id "${NB_UID}" &>/dev/null; then
            echo 'User ${NB_UID} already exists, suspected restart. Skipping user creation...'
            handle_secrets
            handle_kubernetes
            run_code_server "${NB_USER}" "${DATA_DIR}" "$@"
        fi

        # Set UID:GID, then exec sudo as user
        if [[ ! -z "${NB_UID}" && ! -z "${NB_GID}" ]]; then
          echo "Creating group with GID: ${NB_GID}"
          groupadd -g "${NB_GID}" "coders"
          echo "Changing UID to ${NB_UID} and set username to: ${NB_USER}"
          usermod --move-home --home "/home/${NB_USER}" -u "${NB_UID}" -g "${NB_GID}" -l "${NB_USER}" coder
          echo "\$HOME relocated to /home/${NB_USER}, changing directory"
          cd "/home/${NB_USER}"
          if [[ -d "/home/coder" ]]; then
            echo "/home/coder not migrated to /home/${NB_USER}, copying files manually"
            mv {/home/coder/*,/home/coder/.*} "/home/${NB_USER}/"
          fi
          echo "Changing permissions of /home/${NB_USER} to ${NB_UID}:${NB_GID}"
          chown -R "${NB_UID}":"${NB_GID}" "/home/${NB_USER}"
        else
          echo "Set username to: ${NB_USER}"
          usermod -d "/home/${NB_USER}" -l "${NB_USER}" coder
        fi

        if [[ ! -z "${GRANT_SUDO}" ]]; then
          echo "Granting ${NB_USER} sudo access"
          echo "${NB_USER} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/nopasswd
          echo "Adding Python venv bin to sudo secure_path"
          PYTHON_BIN=/opt/python/.venv/bin
          sed -r "s#Defaults\s+secure_path\s*=\s*\"?([^\"]+)\"?#Defaults secure_path=\"\1:${PYTHON_BIN}/bin\"#" /etc/sudoers | grep secure_path > /etc/sudoers.d/path
        fi

        # Git config from env vars
        sudo -E -H -u "${NB_USER}" git config --global user.name "$NB_USER"
        sudo -E -H -u "${NB_USER}" git config --global user.email "$GIT_EMAIL"

        # Symlink secrets
        handle_secrets

        # Create /home/${BUILD_USER} directory so WORKDIR functions on exec
        echo "Creating /home/${BUILD_USER} directory for WORKDIR on Docker exec"
        mkdir -p "/home/${BUILD_USER}"

        handle_kubernetes
        run_code_server "${NB_USER}" "${DATA_DIR}" "$@"

    else

        # Symlink secrets
        export NB_USER=coder
        export NB_UID=1000
        export NB_GID=100
        handle_secrets
        # exec sudo as coder:coder
        echo "No NB_USER variable set. Running as coder:coder, with default data dir"
        run_code_server "coder" "/home/coder" "$@"

    fi

else

    echo "Container is not root. Skipping secrets management. Docker, Kubernetes & Harbor will be unavailable."
    # no root, default run as coder:coder
    echo "Unable to set UID/GID without root. Running as coder:coder, with default data dir"
    mkdir -p "/home/coder/.local/share/code-server/extensions"
    mv "/opt/code-server/extensions" "/home/coder/.local/share/code-server/"
    chown -R coder:coder "/home/coder"
    exec dumb-init /usr/bin/code-server "$@"
fi
